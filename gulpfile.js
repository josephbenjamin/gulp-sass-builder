// Switchstance sass compiler by Joseph B
var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');

var input = './sass/**/*.scss'; // Change to your sass directory
var output = './css'; // Change to your css directory

gulp.task('sass', function () {
  return gulp
    .src(input)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS({compatibility: 'ie9', keepSpecialComments: '*'}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(output));
});

gulp.task('default', ['sass', 'watch']); // Set watch as default

gulp.task('watch', function() {
  return gulp
    .watch(input, ['sass']) // What for change
    .on('change', function(event) {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...'); // Log stuff
    });
});
