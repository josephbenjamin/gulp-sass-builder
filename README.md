# README #

This is a lightweight, super fast Gulp sass compiler using CleanCSS and builds in 30ms. For internal use only. More tools will be added they become part of the Switchstance workflow. 

### Setup ###

Ensure you have the following installed... (https://www.npmjs.com/)
* NPM,
* Gulp,
* Gulp Sass,
* Gulp Clean Css
* Gulp Sourcemaps

### Build instructions ###

Simply install the above tools using command line, copy the *package.json* and *gulpfile.js* into
your project, CD into the project directory and then run *gulp* for sassy goodness.